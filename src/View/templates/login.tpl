            <form class="form-inline" role="form" action="<?= $path ?>/user/login" method="post">
                <input class="form-control mr-sm-2" type="text" placeholder="Login" name="login" aria-label="Login" required>
                <input type="password" class="form-control" placeholder="Password" name="password" aria-label="Password" required>
                <button class="btn btn-outline-secondary ml-2 my-2 my-sm-0" type="submit" aria-label="Login" title="Login"><i class="fas fa-sign-in-alt"></i></button>
            </form>
