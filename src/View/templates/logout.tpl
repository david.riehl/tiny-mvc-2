            <form class="form-inline" role="form" action="<?= $path ?>/user/logout" method="post">
<?php if(isset($login)): ?>
                <i class="fas fa-user mr-2"></i><?= $login ?>
<?php endif; ?>
<?php if(isset($profiles)): ?>
                <i class="fas fa-users mx-2"></i><?= implode(", ", $profiles) ?>
<?php endif; ?>
                <button class="btn btn-outline-secondary ml-2 my-2 my-sm-0" type="submit" aria-label="Logout" title="Logout"><i class="fas fa-sign-out-alt"></i></button>
            </form>
