<?php
namespace MyProject\Kernel;;

use Exception;

class Session
{
    private static $default_values = [];
    public static function setDefaultValues($default_values = [])
    {
        self::$default_values = $default_values;
    }
    private static function loadDefaultValues($empty = false)
    {
        if ($empty) {
            $_SESSION = self::$default_values;
        }
        $_SESSION['user_class'] = self::$user_class;
        $_SESSION['profile_class'] = self::$profile_class;
        $_SESSION['privilege_class'] = self::$privilege_class;
    }

    private static $user_class = null;
    public static function setUserClass($class)
    {
        self::$user_class = $class;
    }

    private static $profile_class = null;
    public static function setProfileClass($class)
    {
        self::$profile_class = $class;
    }

    private static $privilege_class = null;
    public static function setPrivilegeClass($class)
    {
        self::$privilege_class = $class;
    }

    public static function reboot()
    {
        self::unset();
        self::start();
    }

    public static function start()
    {
        session_start();
        self::reset();
        self::checkUserSession();
    }

    private static function reset()
    {
        // loading default values
        $empty = empty($_SESSION);
        self::loadDefaultValues($empty);
    }

    public static function unset()
    {
        session_unset();
        self::reset();
    }

    public static function get($name)
    {
        if (!isset($_SESSION[$name]))
        {
            throw new Exception("Undefined Session variable `{$name}`.");
        }
        return $_SESSION[$name];
    }

    public static function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    private static function checkUserSession()
    {
        // is user class given ?
        if(isset($_SESSION['user_class']))
        {
            $user_class = $_SESSION['user_class'];

            if(! isset($_SESSION['connected'])) {
                throw new Exception("Session variable `connected` not defined, please use setDefaultValues to define it.");
            }
            // Check User Session info only if user is connected
            if ($_SESSION['connected'])
            {
                // Check precondition assertion
                if (! method_exists($_SESSION['user_class'], "getByLogin")) {
                    throw new Exception("Required method `getByLogin` not found in class `{$_SESSION['user_class']}`.");
                }

                // Get login & password from Session
                $login = $_SESSION['user']->login;
                $password = $_SESSION['user']->password;

                // Get user info
                $user = $user_class::getByLogin($login);

                // Check login and password
                if ($user == null || $password != $user->password)
                {
                    // Invalid Session
                    self::unset();
                    $_SESSION['errors'][] = new Error("Error","warning","Invalid session user info.");
                }
                else
                {
                    // Updating User Session
                    $_SESSION['user'] = $user;

                    // is profile class given ?
                    if (isset($_SESSION['profile_class']))
                    {
                        $profile_class = $_SESSION['profile_class'];
                        // Check precondition assertion
                        if (! method_exists($profile_class, "getAll")) {
                            throw new Exception("Method `getAll` not found in class `{$profile_class}`.");
                        }
                        // Update profiles
                        $_SESSION['profiles'] = $profile_class::getAll($user->id);
                    }
                    
                    // is privilege class given ?
                    if (isset($_SESSION['privilege_class'])) 
                    {
                        $privilege_class = $_SESSION['privilege_class'];
                        // Check precondition assertion
                        if (! method_exists($privilege_class, "getAll")) {
                            throw new Exception("Method `getAll` not found in class `{$privilege_class}`.");
                        }
                        // Update privileges
                        $_SESSION['privileges'] = $privilege_class::getAll($user->id);
                    }
                }
            }
        }
    }

    public static function is_assigned($profile)
    {
        return self::property_name_in_array(
            $profile, 
            $_SESSION['profiles']
        );
    }

    public static function is_granted($privilege)
    {
        return self::property_name_in_array(
            $privilege, 
            $_SESSION['privileges']
        );
    }

    public static function property_name_in_array($name, $array)
    {
        $result = false;
        $count = count($array);
        for($i = 0; $i < $count && ! $result; $i++)
        {
            $result = $name == $array[$i]->name;
        }
        return $result;
    }
}