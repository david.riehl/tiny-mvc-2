<?php

namespace MyProject\Kernel;

interface IController
{
    public static function route();
}