<?php
/**
 * Inspired from Weichie Projects
 * @author Bob Weichler <hello@weichie.com>
 * @link https://weichie.com/blog/curl-api-calls-with-php/
 */
class RestApiClient
{

    /**
     * send an HTTP Request
     * @param string $method (POST, GET, PUT, DELETE)
     * @param string $url
     * @param mixed $data (GET: url encoded data, others: JSON format)
     * @param string $api_key authentication token
     * @return 
     */
    public static function sendHttpRequest($method = "GET", $url = "localhost", $data = null, $api_key = null)
    {
        // Preconditions assertions
        if (! in_array($method, ['GET', 'POST', 'PUT', 'DELETE']))
        {
            throw new Exception("Invalid method '{$method}'.");
        }

        // start request

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

        $content_type = 'application/json';
        if (! is_null($data))
        {
            if ($method == "GET")
            {
                $content_type = 'application/x-www-form-urlencoded';
                $url = sprintf("%s?%s", $url, http_build_query($data));
            }
            else
            {
                if (! is_null($data) && is_null(json_decode($data)))
                {
                    throw new Exception('Invalid JSON format.');
                }
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
        }

        curl_setopt($curl, CURLOPT_URL, $url);

        
        $header[] = 'Content-Type: ' . $content_type;
        if (! is_null($api_key))
        {
            $header[] = 'APIKEY: ' . $api_key;
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($curl);
        
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if (! $result)
        {
            throw new Exception("Connection Failure");
        }

        curl_close($curl);
        return $result;
    }
}