<?php
namespace MyProject\Kernel;

use Exception;
use JsonSerializable;

abstract class Entity implements JsonSerializable
{
    abstract public function jsonSerialize();
    public static function jsonDeserialize($json)
    {
        $dataset = json_decode($json);
        if ($dataset === false)
        {
            throw new Exception("Invalid JSON format.");
        }
        $class = get_called_class();
        $object = new $class();
        foreach($dataset as $key => $value)
        {
            $object->$key = $value;
        }
        return $object;
    }
    
    private static function methodName($type, $property)
    {
        $name = strtolower($type);
        $exploded = explode("_", $property);
        foreach($exploded as $item)
        {
            $name .= ucfirst($item);
        }
        return $name;
    }

    public function __get($name)
    {
        // Check property
        $class = get_class($this);
        if (! property_exists($class, $name))
        {
            // property not found
            throw new Exception("Property '{$name}' not found in class '{$class}'.");
        }
        else
        {
            // property found
            // Check method
            $method_name = self::methodName("get", $name);
            if (! method_exists($this, $method_name))
            {
                // method not found
                throw new Exception("Get method not found for the property '{$name}' in class '{$class}'.");
            }
            else
            {
                // Method exists
                return $this->$method_name();
            }
        }
    }

    public function __set($name, $value)
    {
        // Check property
        $class = get_class($this);
        if (! property_exists($class, $name))
        {
            // property not found
            throw new Exception("Property '{$name}' not found in class '{$class}'.");
        }
        else
        {
            // property found
            // Check method
            $method_name = self::methodName("set", $name);
            if (! method_exists($this, $method_name))
            {
                // method not found
                throw new Exception("Set method not found for the property '{$name}' in class '{$class}'.");
            }
            else
            {
                // Method exists
                $this->$method_name($value);
            }
        }
    }
}