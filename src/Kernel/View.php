<?php
namespace MyProject\Kernel;

use Exception;

class View
{
    private static $template = null;
    private static $vars = [];

    public static function setTemplate($template)
    {
        self::$template = $template;
    }

    public static function bindVar($name, $value)
    {
        self::$vars[$name] = $value;
    }

    public static function display()
    {
        if (is_null(self::$template))
        {
            throw new Exception("Template not set.");
        }
        // set template file
        $template = self::$template;

        // set base path from site
        $path = Router::getRootPath();

        // load user session
        if (isset($_SESSION['connected']))
        {
            $connected = $_SESSION['connected'];
            if (isset($_SESSION['user']))
            {
                $login = $_SESSION['user']->nom;
            }
        }

        // set the template vars
        foreach(self::$vars as $name => $value)
        {
            $$name = $value;
        }

        // load template main file
        require_once "src/View/templates/index.tpl";
    }
}