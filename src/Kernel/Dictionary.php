<?php
namespace MyProject\Kernel;

use Countable;
use Iterator;
use Exception;

/**
 * The Dictionary class provides dictionary functionalities for PHP
 */
class Dictionary implements Iterator, Countable
{
    private $index = 0;
    private $count = 0;

    private $keys = [];
    /**
     * Get the keys array
     * @return array
     */
    public function getKeys() : array { return $this->keys; }

    private $values = [];
    /**
     * Get the values array
     * @return array
     */
    public function getValues() : array { return $this->values; }

    private $key_value_pairs = [];

    public function __construct() {
        $this->clear();
    }

    /**
     * Clear the dictionary
     */
    public function clear() : void
    {
        $this->index = 0;
        $this->count = 0;
        $this->keys = [];
        $this->values = [];
        $this->key_value_pairs = [];
    }

    /**
     * Check if the dictionary contains the given key
     * @param object $key
     * @return bool
     */
    public function containsKey(object $key) : bool
    {
        return array_search($key, $this->keys) !== false;
    }

    /**
     * Check if the dictionary contains the given value
     * @param object $value
     * @return bool
     */
    public function containsValue(object $value) : bool
    {
        return array_search($value, $this->values) !== false;
    }

    /**
     * Add a key value pair in dictionary
     * @param object $key
     * @param object $value
     */
    public function add(object $key, object $value) : void
    {
        if ($this->containsKey($key))
        {
            throw new Exception("Key already exists in dictionary.");
        }
        $this->keys[] = $key;
        $this->values[] = $value;
        $this->key_value_pairs[] = (object)[
            "key" => $key,
            "value" => $value,
        ];
        $this->count++;
    }

    /**
     * Get the value associated to the key
     * @param object $key
     * @return object
     */
    public function getValue(object $key) : object
    {
        $index = array_search($key, $this->keys);
        if ($index === false)
        {
            throw new Exception("Key not found in dictionary.");
        }
        return $this->values[$index];
    }

    /**
     * Try to get the value associated to the key
     * @param object $key
     * @param object &$value
     * @return bool true if the key exists
     */
    public function tryGetValue(object $key, object &$value) : bool
    {
        $index = array_search($key, $this->keys);
        $found = $index !== false;
        if ($found)
        {
            $value = $this->values[$index];
        }
        return $found;
    }

    /**
     * Remove the key value pair
     * @param object $key
     * @return bool true is the key is found and removed
     */
    public function remove(object $key) : bool
    {
        $index = array_search($key, $this->keys);
        $found = $index !== false;
        if ($found)
        {
            unset($this->keys[$index]);
            unset($this->values[$index]);
            unset($this->key_value_pairs[$index]);
    
            $this->keys = array_values($this->keys);
            $this->values = array_values($this->values);
            $this->key_value_pairs = array_values($this->key_value_pairs);

            $this->count--;
        }
        return $found;
    }

    /**
     * Countable::count
     */
    public function count() : int
    {
        return $this->count;
    }

    /**
     * Iterator::rewind
     */
    public function rewind() : void {
        $this->index = 0;
    }

    /**
     * Iterator::current
     */
    public function current() : object {
        return $this->key_value_pairs[$this->index];
    }

    /**
     * Iterator::key
     */
    public function key() : int {
        return $this->index;
    }

    /**
     * Iterator::next
     */
    public function next() : void {
        $this->index++;
    }

    /**
     * Iterator::valid
     */
    public function valid() : bool {
        return isset($this->key_value_pairs[$this->index]);
    }

    /**
     * Convert dictionary to string
     * @return string
     */
    public function __toString() : string
    {
        return json_encode($this->key_value_pairs);
    }
}