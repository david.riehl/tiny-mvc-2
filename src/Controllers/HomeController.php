<?php
namespace MyProject\Controllers;

use MyProject\Kernel\Error;
use MyProject\Kernel\IController;
use MyProject\Kernel\Route;
use MyProject\Kernel\Router;
use MyProject\Kernel\View;

class HomeController implements IController
{
    public static function route()
    {
        // Routage secondaire
        $router = new Router();
        $router->addRoute(new Route("/", self::class, "homeAction"));

        $route = $router->findRoute();

        if ($route)
        {
            $route->execute();
        }
        else
        {
            // 404 Error
            $path = Router::getRootPath();
            $route = "/error/404";
            header("location: {$path}{$route}");
        }
    }

    public static function homeAction()
    {
        View::setTemplate("home.tpl");
        View::display();
    }
}