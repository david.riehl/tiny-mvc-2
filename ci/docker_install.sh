#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

# Bash : Print all executed commands
set -x
# Bash : Exit script on command error
set -e

# Update and Upgrade machine
apt-get update -yqq
apt-get upgrade -yqq

#Install Dependencies
apt-get install -yqq git libpq-dev libcurl4-gnutls-dev libicu-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev libonig-dev libzip-dev unzip wget
# Install PHP extensions
docker-php-ext-install mbstring curl json intl gd xml zip bz2 opcache
# Install PHP extension for PostgreSQL
docker-php-ext-install pdo_pgsql
# Install PHP extension for MySQL
docker-php-ext-install pdo_mysql

# Install & enable Xdebug for code coverage reports
pecl install xdebug
docker-php-ext-enable xdebug

# Install and run Composer
curl -sS https://getcomposer.org/installer | php
php composer.phar install

# Install phpunit
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit
